import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';

import { listRenderHold } from '../launch-screen.js';
import './api_record.html';
import './app-not-found.js';

// This is a temporary collection which is holding all the data received from API call
let ApiRecords = new Mongo.Collection('apiRecords');


Template.api_record.onCreated(function() {
  this.state = new ReactiveDict();

  // We will be keeping track of current page in this reactive variable
  this.currentPage = new ReactiveVar(1);

  // Initial states
  this.state.setDefault({
    apiRecords: [],
    currentPage: 1,
    showLoading: false
  });

  // handling scroll position
  const scrollBottom = (()=>{
    $('#content-scrollable').animate({
      scrollTop: $("#content-scrollable").prop("scrollHeight")
    }, 1000);
  });


  this.autorun(()=>{
    // Every time user clicks on 'Load More', we increase the page count and pass it to our publication and subscribe all the records till that page from API.
    let pageNo = this.currentPage.get();
    
    // Subscribing from published api fetched record.
    var searchHandle = Meteor.subscribe('apiRecords', pageNo);
    this.state.set('showLoading', ! searchHandle.ready());

    // Here we are simply checking if we have received data from API call then we are bringing our scroll to the bottom of the page
    if(searchHandle.ready()) scrollBottom();
  })
});


Template.api_record.helpers({

  apiRecords: function() {
    return ApiRecords.find();
  },
  
  showLoading: function() {
    let instance = Template.instance();
    return instance.state.get('showLoading');
  },

  currentPage: function() {
    let instance = Template.instance();
    // We need to pass the instance of currentPage reactive variable's instance to the infinite-load package template. It will get updated from that package. 
    return instance.currentPage;
  }

});
