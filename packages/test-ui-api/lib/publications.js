import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

if (Meteor.isServer) {

  Meteor.publish('apiRecords', function(pageNo) {  
    check(pageNo, Number);
    let environment = Meteor.isProduction?'production':'development';
    let apiDomain = Meteor.settings.public.apis[environment].pcftest;

    try {
      while (pageNo) {
        let response = HTTP.get(`${apiDomain}/wallee/frontpage/frontpages/pageno/${pageNo}`);

        if (response.statusCode === 200 && response.data.exchange && response.data.exchange.frontpageBodyModel && response.data.exchange.frontpageBodyModel.length ) {

            // We are only publising fields that needs to be displayed in our UI
          _.each(response.data.exchange.frontpageBodyModel, (item)=> {
            let doc = {
              seqno: item.seqno,
              hashtag: item.hashtag,
              menutype: item.menutype,
              submenutype: item.submenutype,
              profileimgurl: item.profileimgurl
            };
            this.added('apiRecords', Meteor.uuid(), doc);
          });
        }
        --pageNo;
      }
      
      this.ready();

    } catch(exception) {
      throw new Meteor.Error(exception);
    }
  });

}

