// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by test-ui-api.js.
import { name as packageName } from "meteor/dummy:test-ui-api";

// Write your tests here!
// Here is an example.
Tinytest.add('test-ui-api - example', function (test) {
  test.equal(packageName, "test-ui-api");
});
