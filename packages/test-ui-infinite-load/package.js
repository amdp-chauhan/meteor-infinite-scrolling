Package.describe({
  name: 'dummy:test-ui-infinite-load',
  version: '1.0.0',
  // Brief, one-line summary of the package.
  summary: 'This package is dedicated to provide the dynamic UI infinite-load feature to show the paginated record fetched from APIs',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.5.1');
  api.use('ecmascript');
  api.use(["templating", "underscore"]);
  api.mainModule('test-ui-infinite-load.js');
  
  // We will import all client files here
  api.addFiles([
    "lib/modules/infinite-load.js",
    "lib/templates/infinite-load.html",
  ], ['client']);
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('dummy:test-ui-infinite-load');
  api.mainModule('test-ui-infinite-load-tests.js');
});
