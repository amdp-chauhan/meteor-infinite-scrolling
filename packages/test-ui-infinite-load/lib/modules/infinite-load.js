import { Template } from 'meteor/templating';
import '../templates/infinite-load.html';

Template.infinte_load.events({
  'click .load-more': function(event, instance) {
  	let prevPage = instance.data.page.get();
  	instance.data.page.set(++prevPage);
  }
});