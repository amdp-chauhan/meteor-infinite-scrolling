// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by test-ui-infinite-load.js.
import { name as packageName } from "meteor/dummy:test-ui-infinite-load";

// Write your tests here!
// Here is an example.
Tinytest.add('test-ui-infinite-load - example', function (test) {
  test.equal(packageName, "test-ui-infinite-load");
});
